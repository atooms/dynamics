# Changelog

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 2.0.0 - 2024/10/21

[Full diff](https://framagit.org/atooms/dynamics/-/compare/1.1.2...2.0.0)

###  Changed
- Drop `atooms.api` module, better define drivers and entry points in workflows
- Drop dependence on `atooms-database`
- Use `relaxation_time` from reservior instead of thermostat `mass` in Berendsen backend

###  New
- Add `partial_rmsd` property to backends

###  Bug fixes
- Fix `NosePoincare` initialization of thermostat mass
- Fix possible performance issues with slow I/O
- Require `atooms` 3.20.1 to fix potential issues with views

## 1.1.2 - 2023/07/31

[Full diff](https://framagit.org/atooms/dynamics/-/compare/1.1.1...1.1.2)

###  Bug fixes
- Fix model argument in api so that it accepts a string; require atooms-database

## 1.1.1 - 2023/07/30

[Full diff](https://framagit.org/atooms/dynamics/-/compare/1.1.0...1.1.1)

###  Bug fixes
- Drop dependence on atooms-models, it is enough to have atooms 3.19.0

## 1.1.0 - 2023/06/15

[Full diff](https://framagit.org/atooms/dynamics/-/compare/1.0.0...1.1.0)

###  New
- Add Berendsen thermostat and barostat (require `atooms` 3.17.1)
- Add default imports
- Add new parameters to `api.md()`: `thermo_fields`, `dry`, `fmt`
- Log conserved energy from `api.md()`

###  Bug fixes
- Fix binder setup
- Fix handling temperature in NosePoincare (the temperature parameter was ignored when re-instantiating the backend with the same `System`)
- Fix checkpointing of unfolded positions (via atooms `7104a68`)
- Fix logging


