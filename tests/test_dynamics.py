import unittest
import numpy
from atooms.system import Thermostat, Barostat
from atooms.trajectory import Trajectory, TrajectoryXYZ, change_species
from atooms.simulation import Simulation, Scheduler, write_config, write_thermo, store
from atooms.backends.f90 import Interaction, VerletList
from atooms.dynamics import VelocityVerlet, NosePoincare, LangevinOverdamped, Berendsen


def ave(fname):
    data = numpy.loadtxt(fname, unpack=True)
    return numpy.average(data[2]), numpy.average(data[3])


class Test(unittest.TestCase):

    def setUp(self):
        self.model = {
            "potential": [
	        {
	            "type": "lennard_jones",
	            "parameters": {
		        "sigma": [[1.0, 1.0], [1.0, 1.0]],
		        "epsilon": [[1.0, 1.0], [1.0, 1.0]]
	            }
	        }
            ],	
            "cutoff": [
	        {
	            "type": "cut_shift",
	            "parameters": {"rcut": [[2.5, 2.5], [2.5, 2.5]]}
	        }
            ]
        }

    def test_overhead(self):
        def _silly(sim):
            pass
        trajectory = Trajectory('data/input.xyz')
        system = trajectory[-1]
        system.interaction = Interaction(self.model)
        bck = VelocityVerlet(system, timestep=0.002)
        sim = Simulation(bck, steps=200)
        sim.add(_silly, Scheduler(1))
        sim.run()

    def test_verlet(self):
        trajectory = Trajectory('data/input.xyz')
        system = trajectory[-1]
        system.interaction = Interaction(self.model, neighbor_list=VerletList(skin=0.3))
        bck = VelocityVerlet(system, timestep=0.002)
        sim = Simulation(bck, output_path='/tmp/output.xyz', steps=200)
        sim.trajectory_class = TrajectoryXYZ
        sim.add(write_thermo, Scheduler(10))
        sim.run()
        epot, ekin = ave('/tmp/output.xyz.thermo')
        self.assertAlmostEqual(epot, -2.4, delta=0.2)
        self.assertAlmostEqual(ekin, 6.0, delta=0.2)

    def test_nose_poincare(self):
        trajectory = Trajectory('data/input.xyz')
        system = trajectory[-1]
        system.temperature = 4.0
        system.interaction = Interaction(self.model, neighbor_list=VerletList(skin=0.3))
        system.thermostat = Thermostat(temperature=4.0, mass=5.0, coordinate=1.0, momentum=1.0)
        bck = NosePoincare(system, timestep=0.002)
        sim = Simulation(bck, output_path='/tmp/output.xyz', steps=2000)
        sim.trajectory_class = TrajectoryXYZ
        sim.add(write_thermo, Scheduler(10), ['temperature',
                                              'potential energy per particle',
                                              'conserved energy'],
                functions={'conserved energy': lambda x: x.backend.conserved_energy})
        sim.run()
        data = numpy.loadtxt('/tmp/output.xyz.thermo', unpack=True)
        self.assertAlmostEqual(data[0].mean(), 4.0, delta=0.1)
        self.assertAlmostEqual(data[1].mean(), -2.6, delta=0.1)
        self.assertLess(data[2].std(), 0.2)

    def test_lj(self):
        T = 1.0
        rho = 0.8
        rc = 4.0
        N = 864
        from atooms.system import System
        from atooms.backends.f90 import VerletList
        system = System(N=N)
        system.density = rho
        system.temperature = T
        system.species_layout = 'F'
        model = {
            "potential": [
	        {
	            "type": "lennard_jones",
	            "parameters": {
		        "sigma": [[1.0]],
		        "epsilon": [[1.0]]
	            }
	        }
            ],	
            "cutoff": [
	        {
	            "type": "cut_shift",
	            "parameters": {"rcut": [[rc]]}
	        }
            ]
        }
        system.interaction = Interaction(model, helpers='helpers_3d.f90')
        system.thermostat = Thermostat(temperature=T, mass=5.0)
        bck = NosePoincare(system, timestep=0.004)
        sim = Simulation(bck)
        sim.neighbor_list = VerletList()
        sim.trajectory_class = TrajectoryXYZ
        sim.run(20000)
        sim.add(store, 500, ['temperature',
                             'potential energy per particle',
                             'pressure'])
        sim.run(20000)
        # Reference data from Johnson et al Mol. Phys. 1993
        #print(numpy.mean(sim.data['potential energy per particle']), -5.326)
        #print(numpy.mean(sim.data['pressure']), 1.179)
        # With corrections
        import math
        uc = 4 * (1/rc**12 - 1/rc**6)
        Uc = 8 / 3 * math.pi * system.density * (1./(3*rc**9) - 1/rc**3)
        Uc += 4 / 3 * math.pi * system.density * rc**3 * uc / 2
        Pc = 16 / 3 * math.pi * system.density**2 * (2./(3*rc**9) - 1/rc**3)
        # print(Uc + numpy.mean(sim.data['potential energy per particle']), -5.535)
        # print(Pc + numpy.mean(sim.data['pressure']), 1.011)
        self.assertAlmostEqual(numpy.mean(sim.data['potential energy per particle']), -5.326, places=1)
        self.assertAlmostEqual(numpy.mean(sim.data['pressure']), 1.179, places=1)
        self.assertAlmostEqual(Uc + numpy.mean(sim.data['potential energy per particle']), -5.535, places=1)
        self.assertAlmostEqual(Pc + numpy.mean(sim.data['pressure']), 1.011, places=1)
        
    def test_berendsen_nvt(self):
        trajectory = Trajectory('data/input.xyz')
        system = trajectory[-1]
        system.temperature = 4.0
        system.interaction = Interaction(self.model, neighbor_list=VerletList(skin=0.3))
        system.thermostat = Thermostat(temperature=4.0, relaxation_time=10.0)
        bck = Berendsen(system, timestep=0.002)
        sim = Simulation(bck, output_path='/tmp/output.xyz', steps=2000)
        sim.trajectory_class = TrajectoryXYZ
        sim.add(write_thermo, Scheduler(10), ['temperature',
                                              'potential energy per particle'])
        sim.run()
        data = numpy.loadtxt('/tmp/output.xyz.thermo', unpack=True)
        self.assertAlmostEqual(data[0].mean(), 4.0, delta=0.1)
        self.assertAlmostEqual(data[1].mean(), -2.6, delta=0.1)

    def test_berendsen_npt(self):
        trajectory = Trajectory('data/input.xyz')
        system = trajectory[-1]
        system.temperature = 4.0
        system.interaction = Interaction(self.model)  #, neighbor_list=VerletList(skin=0.3))
        system.thermostat = Thermostat(temperature=4.0, relaxation_time=10.0)
        system.barostat = Barostat(pressure=10.0, relaxation_time=1e4)
        bck = Berendsen(system, timestep=0.001)
        sim = Simulation(bck, output_path='/tmp/output.xyz')
        sim.trajectory_class = TrajectoryXYZ
        sim.run(3000)
        sim.add(store, Scheduler(100), ['temperature', 'pressure',
                                        'potential energy per particle'])
        sim.run(5000)
        self.assertAlmostEqual(numpy.mean(sim.data['temperature']), 4.0, delta=0.1)
        self.assertAlmostEqual(numpy.mean(sim.data['pressure']), 10.0, delta=0.2)
        self.assertAlmostEqual(numpy.mean(sim.data['potential energy per particle']), -2.6, delta=0.1)
        
    def test_bd_cm(self):
        trajectory = Trajectory('data/input.xyz')
        system = trajectory[-1]
        system.interaction = Interaction(self.model, neighbor_list=VerletList(skin=0.3))

        # CM is fixed
        bck = LangevinOverdamped(system, timestep=0.0001, temperature=1.0)
        bck.fixed_cm = True
        sim = Simulation(bck, steps=200)
        cm = sim.backend.system.cm('position_unfolded')
        sim.run()
        cm_end = sim.backend.system.cm('position_unfolded')
        self.assertLess(sum((cm - cm_end)**2)**0.5, 1e-10)

        # Now CM diffuses
        bck.fixed_cm = False
        cm = sim.backend.system.cm('position_unfolded')
        sim.run()
        cm_end = sim.backend.system.cm('position_unfolded')
        self.assertLess(1e-10, sum((cm - cm_end)**2)**0.5)

    def test_pbc(self):
        def inside(s):
            for p in s.system.particle:
                for x, L in zip(p.position, s.system.cell.side):
                    if abs(x) > L/2:
                        raise ValueError('x', x)
        trajectory = Trajectory('data/input.xyz')
        system = trajectory[-1]
        system = change_species(system, 'F')
        system.interaction = Interaction(self.model)
        bck = VelocityVerlet(system, timestep=0.002)
        sim = Simulation(bck, steps=200)
        sim.add(inside, Scheduler(1))
        sim.run()

    def test_rmsd(self):
        from atooms.simulation import store, write_trajectory
        from atooms.system.interaction import InteractionBase
        trajectory = Trajectory('data/input.xyz')
        system = trajectory[-1]
        system.density = 0.1
        for p in system.particle:
            p.velocity[:] = 10 * numpy.ones(3)
        system.interaction = InteractionBase()
        system.compute_interaction("forces")
        data = {}
        bck = VelocityVerlet(system, timestep=0.2)
        sim = Simulation(bck, steps=20)
        sim.add(store, Scheduler(1), what=['rmsd'], data=data)
        sim.run()
        t = bck.timestep * sim.current_step
        self.assertAlmostEqual(data['rmsd'][-1], 3**0.5 * 10 * t)
        # Resetting the backend, restart from t=0
        data = {}
        bck = VelocityVerlet(system, timestep=0.2)
        sim = Simulation(bck)
        sim.add(store, Scheduler(1), what=['rmsd'], data=data)
        sim.run(20)
        t = bck.timestep * sim.current_step
        self.assertAlmostEqual(data['rmsd'][-1], 3**0.5 * 10 * t)
        # Additional steps
        sim.run(40)
        t = bck.timestep * sim.current_step
        self.assertAlmostEqual(data['rmsd'][-1], 3**0.5 * 10 * t)

    def test_partial_rmsd(self):
        self.skipTest('require a more recent version of atooms')
        from atooms.simulation import store, target
        from atooms.system.interaction import InteractionBase
        trajectory = Trajectory('data/input.xyz')
        system = trajectory[-1]
        system.density = 0.1
        for p in system.particle:
            p.velocity[:] = 10 * numpy.ones(3)
        system.interaction = InteractionBase()
        system.compute_interaction("forces")
        data = {}
        bck = VelocityVerlet(system, timestep=0.2)
        sim = Simulation(bck, steps=20)
        sim.add(store, Scheduler(1), what=[('partial_rmsd', lambda sim: sim.backend.partial_rmsd[0])], data=data)
        sim.run()
        t = bck.timestep * sim.current_step
        self.assertAlmostEqual(data['partial_rmsd'][-1], 3**0.5 * 10 * t)

        trajectory = Trajectory('data/kalj-small.xyz')
        system = trajectory[-1]
        system.species_layout = 'F'
        system.interaction = InteractionBase()
        system.compute_interaction("forces")
        data = {}
        bck = VelocityVerlet(system, timestep=0.2)
        sim = Simulation(bck, steps=200)
        sim.add(target, 10, lambda x: min(x.backend.partial_rmsd), 10.0)
        sim.run()
        self.assertTrue(min(sim.backend.partial_rmsd) > 10.0)
        
    def test_ed(self):
        import atooms.system
        import atooms.trajectory
        import atooms.simulation
        from atooms.dynamics.newtonian import EventDriven
        import random
        random.seed(10)
        system = atooms.system.System(N={1: 59, 2: 59})
        system.species_layout = 'F'
        system.density = 0.9
        system.temperature = 1.0
        for p in system.particle:
            if p.species == 1:
                p.radius = 0.5 / 1.4

        bck = EventDriven(system, timestep=0.01)
        sim = atooms.simulation.Simulation(bck)
        sim.run(1000)
        # If we dont reset the steps are wrong
        sim = atooms.simulation.Simulation(bck)
        sim.run(1000)
        self.assertTrue(sum(system.cm_velocity**2)**0.5 < 1e-10)
        self.assertTrue(abs(system.temperature - 1.0) < 1e-10)
        self.assertTrue(system.pressure > 0)

    def test_ed_unf(self):
        import atooms.system
        import atooms.trajectory
        import atooms.simulation
        from atooms.dynamics.newtonian import EventDriven

        import random
        random.seed(1)
        system = atooms.system.System(N={1: 59, 2: 59})
        system.species_layout = 'F'
        system.density = 0.8
        system.temperature = 1.0
        for p in system.particle:
            if p.species == 1:
                p.radius = 0.5 / 1.4
        th = atooms.trajectory.TrajectoryRam()
        bck = EventDriven(system, timestep=0.001)
        sim = atooms.simulation.Simulation(bck)
        # sim.add(atooms.simulation.write_trajectory, 1, trajectory=th)
        sim.run(1000)
        self.assertLess(numpy.max(system.dump('particle.position', flat=True)), system.cell.side[0] / 2)
        self.assertGreater(numpy.max(system.dump('particle.position_unfolded', flat=True)), system.cell.side[0] / 2)
        self.assertAlmostEqual(sim.rmsd, 0.8768126734656285)

if __name__ == '__main__':
    unittest.main()
