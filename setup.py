#!/usr/bin/env python

import os
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

with open('README.md') as f:
    readme = f.read()

setup(name='atooms-dynamics',
      version='2.0.0',
      description='Newtonian and stochastic dynamics backends for atooms',
      long_description=readme,
      long_description_content_type="text/markdown",
      author='Daniele Coslovich',
      author_email='daniele.coslovich@umontpellier.fr',
      url='https://framagit.org/atooms/dynamics',
      packages=['atooms', 'atooms/dynamics'],
      license='GPLv3',
      install_requires=['atooms>=3.20.1', 'f2py-jit>=0.5.0'],
      package_data={'atooms': ['dynamics/*.f90']},
      include_package_data=False,
      zip_safe=False,
      classifiers=[
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.6',
          'Intended Audience :: Science/Research',
          'Topic :: Scientific/Engineering :: Physics',
      ]
     )
