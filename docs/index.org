#+title: Atooms: dynamics
#+author: Daniele Coslovich
#+html_doctype: html5
#+options: html-style:nil
#+setupfile: org.setup
#+property: header-args :comments org :eval no-export :mkdirp yes :results verbatim replace :noweb no-export
#+property: header-args:sh :exports code :prologue ". src/logger.sh; start" :epilogue "stop" :shebang #!/bin/bash
#+property: header-args:python :exports both :shebang "#!/usr/bin/env python" :results output :tangle index.py :session

#+name: fix_path
#+begin_src python :exports none
# This allows execution from the docs/ directory
import sys
if '../' not in sys.path:
    sys.path.insert(0, '../')
#+end_src

#+results: fix_path

#+begin_src python :exports none
# Internal setup
import matplotlib.pyplot as plt
plt.rcParams.update({
    "font.family": "serif",
    'font.size': 11.0,
    'axes.labelsize': 'medium',
    'xtick.major.pad': 2.0,
    'ytick.major.pad': 2.0,
    'xtick.major.size': 4.0,
    'ytick.major.size': 4.0,
    'savefig.bbox': 'tight',
    'savefig.dpi': 180,
    'axes.spines.right': False,
    'axes.spines.top': False,
    'legend.frameon': False,
})
#+end_src

#+results:


* Simulation backends for atooms

#+attr_rst: :directive toctree
#+begin_quote
    index
#+end_quote

This is a demo of running a *molecular dynamics* simulation in the NVT ensemble for the 80-20 Kob-Ansersen binary mixture using the backend shipped with =atooms-dynamics=. We use the symplectic Nosé-Poincaré algorithm to equilibrate the system at normal liquid conditions. The interactions are computed using the built-in Fortran 90 code shipped with =atooms-models=.

This is explicitly written down as a monolitic piece of notebook.

** Setup

Import the necessary modules and turn off verbosity
#+begin_src python
import numpy
import matplotlib
import matplotlib.pyplot as plt
import atooms.core
from atooms.system import Thermostat
from atooms.simulation import Simulation, Scheduler, write_trajectory, store
from atooms.dynamics import NosePoincare, VelocityVerlet
from atooms.trajectory import TrajectoryXYZ

atooms.core.progress.active = False
atooms.core.utils.setup_logging(level=40, update=True)
#+end_src

#+results:

** Create the starting configuration

We create a small system of $N=100$ particles at the canonical density $\rho=1.2$ and composition 80-20
#+begin_src python
from atooms.system import System
system = System(N=100)
system.density = 1.2
system.composition = {'A': 80, 'B': 20}
print(system)
#+end_src

#+results:
: system composed by N=100 particles
: with chemical composition C={'A': 80, 'B': 20}
: with chemical concentration x={'A': 0.8, 'B': 0.2}
: enclosed in a cubic box at number density rho=1.200000

** Define the interaction model

We look for the interaction model of the Kob-Andersen mixture in the =atooms.models= database.
#+begin_src python
from atooms import models
from pprint import pprint
pprint(models.get('kob_andersen'))
#+end_src

#+results:
: {'cutoff': [{'parameters': {'rcut': [[2.5, 2.0], [2.0, 2.2]]},
:              'type': 'cut_shift'}],
:  'potential': [{'parameters': {'epsilon': [[1.0, 1.5], [1.5, 0.5]],
:                                'sigma': [[1.0, 0.8], [0.8, 0.88]]},
:                 'type': 'lennard_jones'}],
:  'reference': 'W. Kob and H. C. Andersen, Phys. Rev. Lett. 73, 1376 (1994)'}

We define a new model, changing the cutoff to a smoother one, and set up the interaction
#+begin_src python
model = {'cutoff': [{'parameters': {'rcut': [[2.5, 2.0], [2.0, 2.2]]},
                     'type': 'linear_cut_shift'}],
         'potential': [{'parameters': {'epsilon': [[1.0, 1.5], [1.5, 0.5]],
                                       'sigma': [[1.0, 0.8], [0.8, 0.88]]},
                        'type': 'lennard_jones'}],
}
system.species_layout = 'F'  # This ensures species are Fortran-styled
system.interaction = models.f90.Interaction(model)
# system.interaction.neighbor_list = f90.VerletList(skin=0.3)
#+end_src

#+results:
: >>>

** Run the simulation

We create a simulation backend that integrates the equation of motions using the Nosé-Poincaré algorithm at a temperature $T=2.0$. The simulation s fairly short because at this temperature the system is a normal liquid and thermalies rapidly. We also add  
#+begin_src python
system.set_temperature(2.0)
system.thermostat = Thermostat(temperature=2.0, mass=5.0)
bck = NosePoincare(system, timestep=0.002)
sim = Simulation(bck)
data = {}
# This adds a callback to store every 100 steps several system attributes in the data dictionary
# Check the atooms.simulation.observers.store() doc for more info.
sim.add(store, 100, ['steps', 'potential energy per particle', 
                     'kinetic energy per particle', 'temperature'], data)
#+end_src

#+results:

Run the simulation and show a snapshot of the final state of the system (this requires =ovito=)
#+begin_src python
sim.run(int(1e4))

# Temporarily set alphabetical species for ovito
system.species_layout = 'A'
# system.show('ovito', outfile='_images/config.png')
system.species_layout = 'F'
#+end_src

#+results:

[[./_images/config.png]]

Check that the kinetic temperature looks good
#+begin_src python :var figname="_images/kinetic.png" :results value file :prologue "import matplotlib.pyplot as plt" :epilogue "plt.savefig(f'{figname}', bbox_inches='tight'); plt.clf(); f'{figname}'"
plt.plot(data['steps'], data['temperature'], 'o', label='Kinetic temperature')
plt.plot(data['steps'], [system.thermostat.temperature]*len(data['steps']), '-', label='Thermostat temperature')
plt.xlabel('Steps')
#+end_src

#+results:
[[file:_images/kinetic.png]]

We now do a production run in which we store configurations in a trajectory file, written in xyz format. This time we store the configurations using an exponential sampling, which allows to resolve both short and long time dynamics. This will take about a minute.
#+begin_src python :async
th = TrajectoryXYZ('/tmp/config.xyz', 'w')
bck = VelocityVerlet(system, timestep=0.006)
sim = Simulation(bck)
# This adds a callback to write the system in the th trajectory using an exponential schedule
sim.add(write_trajectory, Scheduler(block=[2**n for n in range(10)]),
                                    variables=['species', 'pos', 'vel'], trajectory=th)
data = {}
sim.add(store, 100, ['steps', 'potential energy per particle', 
                     'total energy per particle', 'temperature'], data)
sim.run(int(1e5))
th.close()
#+end_src

#+results:

Chech that the total energy is conserved
#+begin_src python :var figname="_images/total.png" :results value file :prologue "import matplotlib.pyplot as plt" :epilogue "plt.savefig(f'{figname}', bbox_inches='tight'); plt.clf(); f'{figname}'"
plt.plot(data['steps'], data['total energy per particle'], '-', label='Total')
plt.plot(data['steps'], data['potential energy per particle'], '-', label='Potential')
plt.xlabel('Steps')
#+end_src

#+results:
[[file:_images/total.png]]


** Analyze the trajectory file

The trajectory file can be analyzed using the =atooms-postprocessing= package. We compute the velocity auto-correlation function $Z(t)$ over a short time grid
#+begin_src python :var figname="_images/vacf.png" :results value file :prologue "import matplotlib.pyplot as plt" :epilogue "plt.savefig(f'{figname}', bbox_inches='tight'); plt.clf(); f'{figname}'"
from atooms.postprocessing import VelocityAutocorrelation

with TrajectoryXYZ('/tmp/config.xyz') as th:
    cf = VelocityAutocorrelation(th, tgrid=th.times[:9])
    cf.compute()
plt.plot(cf.grid, cf.value, '-o')
plt.xlabel('t')
plt.ylabel('Z(t)')
#+end_src

#+results:
[[file:_images/vacf.png]]

Here we compute instead the mean square displacement
#+begin_src python :var figname="_images/msd.png" :results value file :prologue "import matplotlib.pyplot as plt" :epilogue "plt.savefig(f'{figname}', bbox_inches='tight'); plt.clf(); f'{figname}'"
from atooms.postprocessing import MeanSquareDisplacement

with TrajectoryXYZ('/tmp/config.xyz') as th:
    cf = MeanSquareDisplacement(th)
    cf.compute()
plt.plot(cf.grid[:20], cf.value[:20], '-o')
plt.xlabel('t')
plt.ylabel('MSD')
#+end_src

#+results:
[[file:_images/msd.png]]

** Notes :noexport:
#+begin_src python :prologue "plt.clf()" :epilogue "plt.savefig(image, bbox_inches='tight'); print(image); del image" :results file :var image="/tmp/u.png"
from atooms import models
from atooms.system import Cell, Particle, System
s = System()
s.particle = [Particle(), Particle()]
s.cell = Cell([1000, 1000, 1000])
s.particle[0].position = numpy.array([0.,0.,0.])
s.particle[1].position = numpy.array([1.,0.,0.])
model = {'cutoff': [{'parameters': {'rcut': [[2.5]]},
                     'type': 'linear_cut_shift'}],
         'potential': [{'parameters': {'epsilon': [[1.0]],
                                       'sigma': [[1.0]]},
                        'type': 'lennard_jones'}],
}
s.species_layout = 'F'  # This ensures species are Fortran-styled
s.interaction = models.f90.Interaction(model)
import numpy
x, u = [], []
for xi in numpy.linspace(2.3, 2.5, 30):
    s.particle[1].position[0] = xi
    s.compute_interaction("forces")
    u.append(s.interaction.forces[0][0])  #.potential_energy(per_particle=True))
    x.append(xi)
import matplotlib.pyplot as plt
plt.plot(x, u, '-')
#+end_src

#+results:
[[file:/tmp/u.png]]
